export enum IngredientUnit {
    cl = 'cl',
    dash = 'dash',
    pinch = 'pinch',
    tsp = 'teaspoon',
}
