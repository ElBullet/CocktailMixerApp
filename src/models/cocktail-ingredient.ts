import { Ingredient } from './ingredient';

export class CocktailIngredient {
    cocktail_id: number;
    ingredient_id: number;
    ingredient?: Ingredient;
    amount?: number;
}
