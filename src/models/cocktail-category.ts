export enum CocktailCategory {
    creamy = 'Creamy',
    classic = 'Classic',
    longdrink = 'Longdrink',
    martini = 'Martini',
    tropical = 'Tropical',
}
