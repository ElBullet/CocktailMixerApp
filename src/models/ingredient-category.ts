export enum IngredientCategory {
    alcohol = 'Alcohol',
    juice = 'Juice',
    syrup = 'syrup',
    softdrink = 'Softdrink',
    other = 'Other'
}
