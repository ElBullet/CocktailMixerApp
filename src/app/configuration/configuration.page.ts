import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { ChooseIngredientPage } from '../choose-ingredient/choose-ingredient.page';
import { OverlayEventDetail } from '@ionic/core';
import { Ingredient } from 'src/models/ingredient';
import { IngredientCategory } from 'src/models/ingredient-category';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.page.html',
  styleUrls: ['./configuration.page.scss'],
})
export class ConfigurationPage implements OnInit {
  public ingredients: Ingredient[] = [
    {
      id: 1,
      name: 'Cola',
      category: IngredientCategory.softdrink,
      image_src: 'https://dtgxwmigmg3gc.cloudfront.net/files/589d342e777a4239b9097bd9-icon-256x256.png',
    },
    {
      id: 2,
      name: 'White Rum',
      category: IngredientCategory.alcohol,
      image_src: 'https://dtgxwmigmg3gc.cloudfront.net/files/5b179d9de1272f6f6101b27c-icon-256x256.png',
    },
    {
      id: 3,
      name: 'Orangensaft',
      category: IngredientCategory.juice,
      image_src: 'https://dtgxwmigmg3gc.cloudfront.net/files/5b48c1ede1272f7b5e03f091-icon-256x256.png',
    },
    {
      id: 4,
      name: 'Vodka',
      category: IngredientCategory.alcohol,
      image_src: 'https://dtgxwmigmg3gc.cloudfront.net/files/5257fc4dc566d757c0008ac0-icon-256x256.png',
    },
    {
      id: 5,
      name: 'Lime Juice',
      category: IngredientCategory.juice,
      image_src: 'https://dtgxwmigmg3gc.cloudfront.net/files/5ba9d940777a420aff035b20-icon-256x256.png',
    },
    null,
  ];

  constructor(private popoverController: PopoverController) { }

  ngOnInit() {
  }

  public chooseIngredient(clickEvent: Event, index: number) {
    this.popoverController.create({
      component: ChooseIngredientPage,
      componentProps: {
        filter: {
          unit: 'cl'
        },
      },
      event: clickEvent,
      animated: true,
    }).then(popover => {
      popover.onDidDismiss().then((event: OverlayEventDetail<Ingredient>) => {
        this.ingredients[index] = event.data;
      });
      popover.present();
    });
  }

}
