import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ConfigurationPage } from './configuration.page';
import { ChooseIngredientPageModule } from '../choose-ingredient/choose-ingredient.module';

const routes: Routes = [
  {
    path: '',
    component: ConfigurationPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChooseIngredientPageModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ConfigurationPage]
})
export class ConfigurationPageModule {}
