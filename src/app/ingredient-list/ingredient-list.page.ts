import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Ingredient } from 'src/models/ingredient';
import { IngredientService } from '../services/ingredient.service';
import { ModalController } from '@ionic/angular';
import { IngredientAddPage } from '../ingredient-add/ingredient-add.page';

@Component({
  selector: 'app-ingredient-list',
  templateUrl: 'ingredient-list.page.html',
  styleUrls: ['ingredient-list.page.scss']
})
export class IngredientListPage {
  public ingredientsObservable: Observable<Ingredient[]>;

  constructor(private ingredientService: IngredientService, private modalController: ModalController) {
    this.ingredientsObservable = this.ingredientService.getAll();
  }

  ionViewDidLoad() { }

  public openAddModal() {
    this.modalController.create({
      component: IngredientAddPage,
    }).then(modal => {
      modal.present();
      modal.onDidDismiss()
        .then(() => this.ingredientsObservable = this.ingredientService.getAll());
    });
  }
}
