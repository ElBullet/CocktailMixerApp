import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IngredientListPage } from './ingredient-list.page';

describe('IngredientListPage', () => {
  let component: IngredientListPage;
  let fixture: ComponentFixture<IngredientListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IngredientListPage],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngredientListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
