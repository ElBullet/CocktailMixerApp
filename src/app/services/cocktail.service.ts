import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Cocktail } from 'src/models/cocktail';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { CocktailIngredient } from 'src/models/cocktail-ingredient';

@Injectable({
  providedIn: 'root'
})
export class CocktailService {

  constructor(private http: HttpClient) { }

  public getAll(): Observable<Cocktail[]> {
    return this.http.get<Cocktail[]>(`${environment.apiUrl}/cocktails`);
  }

  public getById(id: number): Observable<Cocktail> {
    return this.http.get<Cocktail>(`${environment.apiUrl}/cocktails/${id}`);
  }

  public getIngredients(id: number): Observable<CocktailIngredient[]> {
    return this.http.get<CocktailIngredient[]>(`${environment.apiUrl}/cocktails/${id}/ingredients`);
  }

  public create(cocktail: Cocktail): Observable<Cocktail> {
    return this.http.post<Cocktail>(`${environment.apiUrl}/cocktails`, cocktail);
  }

  public update(cocktail: Cocktail): Observable<boolean> {
    return this.http.put(`${environment.apiUrl}/cocktails/${cocktail.id}`, cocktail).pipe(map(() => true));
  }

  public delete(cocktailId: number): Observable<boolean> {
    return this.http.delete(`${environment.apiUrl}/cocktails/${cocktailId}`).pipe(map(() => true));
  }
}
