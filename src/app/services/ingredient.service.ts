import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Ingredient } from 'src/models/ingredient';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class IngredientService {

  constructor(private http: HttpClient) { }

  public getAll(filter?: Ingredient): Observable<Ingredient[]> {
    if (filter) {
      const query = Object.keys(filter).map(function(key) {
        return `${key}=${filter[key]}`;
      }).join('&');
      return this.http.get<Ingredient[]>(`${environment.apiUrl}/ingredients?${query}`);
    }

    return this.http.get<Ingredient[]>(`${environment.apiUrl}/ingredients`);
  }

  public getById(id: number): Observable<Ingredient> {
    return this.http.get<Ingredient>(`${environment.apiUrl}/ingredients/${id}`);
  }

  public create(ingredient: Ingredient): Observable<Ingredient> {
    return this.http.post<Ingredient>(`${environment.apiUrl}/ingredients`, ingredient);
  }

  public update(ingredient: Ingredient): Observable<boolean> {
    return this.http.put(`${environment.apiUrl}/ingredients/${ingredient.id}`, ingredient).pipe(map(() => true));
  }

  public delete(ingredientId: number): Observable<boolean> {
    return this.http.delete(`${environment.apiUrl}/ingredients/${ingredientId}`).pipe(map(() => true));
  }
}
