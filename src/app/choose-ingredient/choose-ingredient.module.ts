import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChooseIngredientPage } from './choose-ingredient.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
  ],
  entryComponents: [ChooseIngredientPage],
  declarations: [ChooseIngredientPage]
})
export class ChooseIngredientPageModule {}
