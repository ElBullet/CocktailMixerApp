import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChooseIngredientPage } from './choose-ingredient.page';

describe('ChooseIngredientPage', () => {
  let component: ChooseIngredientPage;
  let fixture: ComponentFixture<ChooseIngredientPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChooseIngredientPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseIngredientPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
