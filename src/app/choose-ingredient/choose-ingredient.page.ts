import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Ingredient } from 'src/models/ingredient';
import { IngredientService } from 'src/app/services/ingredient.service';
import { PopoverController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-choose-ingredient',
  templateUrl: './choose-ingredient.page.html',
  styleUrls: ['./choose-ingredient.page.scss'],
})
export class ChooseIngredientPage implements OnInit {
  private onInputDelay: number;
  private filter: Ingredient;
  public ingredientsObservable: Observable<Ingredient[]>;

  constructor(private popoverController: PopoverController,
    private ingredientService: IngredientService,
    navParams: NavParams) {
    this.filter = navParams.data.filter;
  }

  ngOnInit() {
  }

  public onInput(value: string) {
    if (value.length > 2) {
      window.clearTimeout(this.onInputDelay);
      this.onInputDelay = window.setTimeout(() => {
        const filter = new Ingredient;
        filter.name = value;
        for (const prop of Object.keys(this.filter)) {
          filter[prop] = this.filter[prop];
        }
        this.ingredientsObservable = this.ingredientService.getAll(filter);
      }, 500);
    }
  }

  public selectIngredient(ingredient: Ingredient) {
    this.popoverController.dismiss(ingredient);
  }

}
