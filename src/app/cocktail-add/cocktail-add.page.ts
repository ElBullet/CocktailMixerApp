import { Component, OnInit } from '@angular/core';
import { ModalController, PopoverController } from '@ionic/angular';
import { CocktailCategory } from 'src/models/cocktail-category';
import { Cocktail } from 'src/models/cocktail';
import { ChooseIngredientPage } from '../choose-ingredient/choose-ingredient.page';
import { CocktailService } from '../services/cocktail.service';
import { OverlayEventDetail } from '@ionic/core';
import { Ingredient } from 'src/models/ingredient';

@Component({
  selector: 'app-cocktail-add',
  templateUrl: './cocktail-add.page.html',
  styleUrls: ['./cocktail-add.page.scss'],
})
export class CocktailAddPage implements OnInit {
  public model = new Cocktail;
  public categories = Object.keys(CocktailCategory);

  constructor(private modalController: ModalController,
    private popoverController: PopoverController,
    private cocktailService: CocktailService) {
      this.model.ingredients = [];
  }

  ngOnInit() {
  }

  public addIngredient(clickEvent: Event) {
    this.popoverController.create({
      component: ChooseIngredientPage,
      translucent: true,
      event: clickEvent
    }).then(popover => {
      popover.onDidDismiss().then((event: OverlayEventDetail<Ingredient>) => {
        if (event.data) {
          this.model.ingredients.push({
            ingredient: event.data,
            ingredient_id: event.data.id,
            cocktail_id: undefined,
          });
        }
      });
      popover.present();
    });
  }

  public close() {
    this.modalController.dismiss();
  }

  public submit() {
    this.cocktailService.create(this.model).subscribe(() => this.modalController.dismiss());
  }
}
