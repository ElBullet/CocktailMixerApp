import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CocktailAddPage } from './cocktail-add.page';
import { ChooseIngredientPageModule } from '../choose-ingredient/choose-ingredient.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChooseIngredientPageModule,
  ],
  entryComponents: [CocktailAddPage],
  declarations: [CocktailAddPage],
})
export class CocktailAddPageModule {}
