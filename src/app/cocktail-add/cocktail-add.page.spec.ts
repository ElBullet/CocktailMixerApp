import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CocktailAddPage } from './cocktail-add.page';

describe('CocktailAddPage', () => {
  let component: CocktailAddPage;
  let fixture: ComponentFixture<CocktailAddPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CocktailAddPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CocktailAddPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
