import { Component } from '@angular/core';
import { CocktailIngredient } from 'src/models/cocktail-ingredient';
import { CocktailService } from '../services/cocktail.service';
import { Cocktail } from 'src/models/cocktail';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cocktail-details',
  templateUrl: './cocktail-details.page.html',
  styleUrls: ['./cocktail-details.page.scss'],
})
export class CocktailDetailsPage {
  public cocktail: Cocktail;
  public ingredients: CocktailIngredient[];

  constructor(private route: ActivatedRoute,
    private cocktailService: CocktailService) {
    this.route.params.subscribe(params => {
      this.cocktailService.getById(params.id).subscribe(cocktail => this.cocktail = cocktail);
      this.cocktailService.getIngredients(params.id).subscribe(ingredients => this.ingredients = ingredients);
    });
  }

  public formatAmount(ci: CocktailIngredient) {
    if (ci.amount) {
      if (ci.ingredient && ci.ingredient.unit) {
        return ci.amount + ' ' + ci.ingredient.unit;
      } else {
        return ci.amount.toString();
      }
    }
    return '';
  }

}
