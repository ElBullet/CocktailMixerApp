import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CocktailDetailsPage } from './cocktail-details.page';

const routes: Routes = [
  {
    path: '',
    component: CocktailDetailsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CocktailDetailsPage]
})
export class CocktailDetailsPageModule {}
