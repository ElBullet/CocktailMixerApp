import { Component, OnInit } from '@angular/core';
import { IngredientCategory } from 'src/models/ingredient-category';
import { IngredientUnit } from 'src/models/ingredient-unit';
import { ModalController } from '@ionic/angular';
import { IngredientService } from '../services/ingredient.service';
import { Ingredient } from 'src/models/ingredient';

@Component({
  selector: 'app-ingredient-add',
  templateUrl: './ingredient-add.page.html',
  styleUrls: ['./ingredient-add.page.scss'],
})
export class IngredientAddPage implements OnInit {
  public categories = Object.keys(IngredientCategory);
  public units = Object.keys(IngredientUnit);
  public model = new Ingredient;

  constructor(private ingredientService: IngredientService, private modalController: ModalController) { }

  ngOnInit() {
  }

  public submit() {
    this.ingredientService.create(this.model).subscribe(() => {
      this.close();
    });
  }

  public close() {
    this.modalController.dismiss();
  }
}
