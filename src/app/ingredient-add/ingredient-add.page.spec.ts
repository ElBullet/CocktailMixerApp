import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IngredientAddPage } from './ingredient-add.page';

describe('IngredientAddPage', () => {
  let component: IngredientAddPage;
  let fixture: ComponentFixture<IngredientAddPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngredientAddPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngredientAddPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
