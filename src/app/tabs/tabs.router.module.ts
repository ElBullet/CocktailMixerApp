import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'home',
        loadChildren: '../home/home.module#HomePageModule'
      },
      {
        path: 'ingredients',
        loadChildren: '../ingredient-list/ingredient-list.module#IngredientListPageModule'
      },
      {
        path: 'cocktails',
        loadChildren: '../cocktail-list/cocktail-list.module#CocktailListPageModule'
      },
      {
        path: 'cocktails/:id',
        loadChildren: '../cocktail-details/cocktail-details.module#CocktailDetailsPageModule'
      },
      {
        path: 'configuration',
        loadChildren: '../configuration/configuration.module#ConfigurationPageModule'
      },
    ],
  },
  {
    path: '',
    redirectTo: 'tabs/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
