import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TabsPageRoutingModule } from './tabs.router.module';

import { TabsPage } from './tabs.page';
import { CocktailAddPageModule } from '../cocktail-add/cocktail-add.module';
import { IngredientAddPageModule } from '../ingredient-add/ingredient-add.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    TabsPageRoutingModule,
    CocktailAddPageModule,
    IngredientAddPageModule,
  ],
  declarations: [TabsPage]
})
export class TabsPageModule {}
