import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CocktailListPage } from './cocktail-list.page';

describe('CocktailListPage', () => {
  let component: CocktailListPage;
  let fixture: ComponentFixture<CocktailListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CocktailListPage],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CocktailListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
