import { Component } from '@angular/core';
import { Cocktail } from 'src/models/cocktail';
import { CocktailService } from '../services/cocktail.service';
import { Observable } from 'rxjs';
import { ModalController } from '@ionic/angular';
import { CocktailAddPage } from '../cocktail-add/cocktail-add.page';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cocktail-list',
  templateUrl: 'cocktail-list.page.html',
  styleUrls: ['cocktail-list.page.scss']
})
export class CocktailListPage {
  public cocktailsObservable: Observable<Cocktail[]>;

  constructor(private cocktailService: CocktailService,
    private modalController: ModalController) {
    this.cocktailsObservable = this.cocktailService.getAll();
  }

  ionViewDidLoad() {
  }

  public openAddModal() {
    this.modalController.create({
      component: CocktailAddPage,
    }).then(modal => {
      modal.present();
      modal.onDidDismiss()
        .then(() => this.cocktailsObservable = this.cocktailService.getAll());
    });
  }
}
